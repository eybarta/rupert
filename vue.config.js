// vue.config.js

const path = require("path");
const webpack = require("webpack");
module.exports = {
  lintOnSave: true,
  // devServer: {
  //   proxy: {
  //     '^/api': {
  //       target: 'http://localhost:3080/',
  //       ws: true,
  //       changeOrigin: true
  //     }
  //   }
  // },
  configureWebpack: {
    resolve: {
      alias: {
        mixins: __dirname + "./src/styl/mixins.styl",
        rupture: __dirname + "/node_modules/rupture/rupture/index.styl"
      }
    },
    plugins: [
      // new LodashModuleReplacementPlugin(),
      // new webpack.ProvidePlugin({
      //   _: "lodash"
      // }),
      new webpack.LoaderOptionsPlugin({
        options: {
          stylus: {
            import: [__dirname + "/src/styl/mixins.styl"]
          }
        }
      })
    ]
  },
  chainWebpack: config => {
    const types = ["vue-modules", "vue", "normal-modules", "normal"];
    config.module
      .rule("vue")
      .use("vue-loader")
      .loader("vue-loader")
      .tap(options => {
        options.prettify = true;
        return options;
      });
    types.forEach(type =>
      addStyleResource(config.module.rule("stylus").oneOf(type))
    );
  }
};
function addStyleResource(rule) {
  rule
    .use("style-resource")
    .loader("style-resources-loader")
    .options({
      patterns: [
        path.resolve(__dirname, "./node_modules/rupture/rupture/index.styl"),
        path.resolve(__dirname, "./src/styl/variables.styl")
      ]
    });
}