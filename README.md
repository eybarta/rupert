# rupert

## Project setup
```
npm install
```

### Run local server at :8081
```
npm run serve
```

### Run proxy server at :3080
```
npm run server
```

## Stack used for this exercise:
```
Vue, Vuex, VueRouter, Stylus (created with vue-cli)
```

## NOTES

For the sake of a timely submission of this exercise,  
there's a notable mix of CSS writing styles.  
Of course in a production project there would be  
a uniform style guide followed.  

#### Improvements to be made:

- Smoother behaviour for the change of data in the SidePanel.  
- Better validations for the sign-in popup.  
- Handle errors when imageUrl doesn't exist.  
- Better error handling in general.  
- And more!



