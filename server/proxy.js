const express = require('express');
const  {createProxyMiddleware} = require('http-proxy-middleware');
const app = express();
const cors = require('cors');
const port = 3080;
// proxy middleware options
const proxyOptions = {
  target: 'https://10ax.online.tableau.com', // target host
  changeOrigin: true,
  onProxyReq(proxyReq, req, res) {
    proxyReq.setHeader('Accept', 'application/json');
    proxyReq.setHeader('Content-Type', 'application/json');
  }
};

// USE CORS (allow origin...)
const allowedOrigins = ['http://localhost:8081'];
const corOptions = {
  origin: allowedOrigins
};
app.use(cors(corOptions))

// USE PROXY
app.use('/api', createProxyMiddleware(proxyOptions))
app.listen(port, () => {
  console.log(`Server listening @ ${port}`);
});