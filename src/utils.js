
/*
    STORAGE 
    CHECK SUPPORT
*/
function storageSupport() {
  try {
    return "sessionStorage" in window && window["sessionStorage"] !== null;
  } catch (e) {
    return false;
  }
}

// FETCH FROM STORAGE
/*
  For now all storage will use SESSION,
  unless we choose otherwise
*/
function getFromStorage(key, storageType = sessionStorage) {
  if (storageSupport()) {
    return storageType.getItem(key);
  }
  return null;
}
// SAVE TO STORAGE
function setToStorage(key, data, storageType = sessionStorage) {
  typeof data !== "string" && (data = JSON.stringify(data));
  storageSupport() && storageType.setItem(key, data);
}

// CHECK EXPIRATION
// function checkStorageExpiration(storageType, key) {
//   let curdate = new Date(),
//     date = new Date(storageType.getItem(key + "_expiration"));
//   if (!!date && curdate > date) {
//     removeFromStorage(storageType, key);
//     return true;
//   }
//   return false;
// }
// SET EXPIRATION
// function setStorageExpiration(storageType, key, days = 10) {
//   let expiredate = new Date();
//   expiredate.setDate(expiredate.getDate() + days);
//   storageType.setItem(key + "_expiration", expiredate);
// }

// REMOVE FROM STORAGE
function removeFromStorage(key, storageType = sessionStorage) {
  storageType.removeItem(key);
  // storageType.removeItem(key + "_expiration");
}

// RESTART CACHING
// function clearCache() {
//   // removeFromStorage(localStorage, "_marketcategories");
//   // removeFromStorage(sessionStorage, "_filtersUI");
//   localStorage.clear();
//   sessionStorage.clear();
// }

export const storage = {
  support: storageSupport,
  get: getFromStorage,
  set: setToStorage,
  remove: removeFromStorage
};
