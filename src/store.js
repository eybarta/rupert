import Vue from "vue";
import Vuex from "vuex";
import { storage } from "@/utils";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    apiBase: 'http://localhost:3080',
    user: null,
    sourceData: null,
    searchTerm: null,
    sort: null
  },
  mutations: {
    UPDATE_USER(state, user) {
      state.user = user
    },
    UPDATE_SEARCH_TERM(state, term) {
      state.searchTerm = term;
    },
    UPDATE_SORT(state, payload) {
      state.sort = payload;
    },
    UPDATE_SOURCE_DATA(state, payload) {
      state.sourceData = payload;
    }
  },
  actions: {
    updateUser({commit}, payload) {
      commit("UPDATE_USER", payload);
    },
    updateSearchTerm({commit}, term) {
      commit("UPDATE_SEARCH_TERM", term);
    },
    updateSort({commit}, payload) {
      commit("UPDATE_SORT", payload);
    },
    fetchSourceData({state, commit, dispatch}) {
      let token = state.user.token;
      let siteId = state.user.siteId;
      // let fields = "owner.name,owner.fullName,owner.siteRole,owner.lastLogin,owner.email,workbook.description,workbook.name,workbook.contentUrl,workbook.showTabs,workbook.size";
      let api = `${state.apiBase}/api/3.12/sites/${siteId}/views?includeUsageStatistics=true&fields=_all_`;
      return fetch(api, {
        method: "GET",
        headers: {
          "Accept" : "application/json",
          "Content-Type" : "application/json",
          "Access-Control-Allow-Origin": "*",
          "X-Tableau-Auth": token
        },
      }).then(res => {
        return res.json() 
      })
      .then(response => {
        if (response.error && response.error.code ==="401002") {
          // TOKEN EXPIRED PROBABLY...
          storage.remove("tableau_user");
          dispatch("updateUser", null)
        }
        else {
          commit("UPDATE_SOURCE_DATA", response)
        }
        return response
        
      })
      .catch(err => {
        console.log('err: ', err);
      })
    }
  },
  getters: {

  }
})