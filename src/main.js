/*
  fetch polyfill (needed for older versions of Internet Explorer)
*/
import "whatwg-fetch";

import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import router from "./router";
import store from "./store";
import "./app.styl";

// TOOLTIP
// https://akryum.github.io/v-tooltip/#/
import VTooltip from "v-tooltip";
Vue.use(VTooltip);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
